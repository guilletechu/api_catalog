#!/usr/bin/env node
'use strict';

const app = require('./app.js');
const meow = require('meow');

const cli = meow({
    help: [
        'Description',
        '   Gen API Catalog client tool to generate structure.json (API Catalog structure)',
        '   By default it generates the structure.json in gen-site/src/structure.json',
        '',
        'Usage',
        '   node cli.js',
        '',
    ]
});

app.createApiCatalog();
