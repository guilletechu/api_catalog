'use strict';

const fs = require('fs-extra'),
    _ = require('lodash'),
    request = require('request'),
    Q = require('q'),
    requestPromise = require('request-promise'),
    logger = require('../logger/logger.js');

const merge = require('../utils/deepmerge.js');

var commonsRepos = ['glapi-global-apis-commons-commons', 'glapi-global-apis-commons-types', 'global-apis-commons-commons-v0', 'global-apis-commons-types-v0']

function obtenerProxy(){
  try {
    return fs.readFileSync('../.proxy', 'utf8');
  }catch (err){
    return "";
  }
}

let proxyData = obtenerProxy();


function obtainDomainName(domain) {
    var domainName = domain.substring(domain.lastIndexOf('-') + 1, domain.length)
    return domainName;
}

function trimRepoProperties(repository) {
    var domainRegex = /(\w*)-/;
    var browseRegex = /\/browse/;

    // we remove the prefix GLAPI, ARAPI, CLAPI, etc
    var repoName = repository.name.replace(domainRegex.exec(repository.name)[0], '');

    // we remove the sufix /browse from the url
    // and the prefix https://globaldevtools.bbva.com
    var url = repository.links.self[0].href;
    var repoUrl = url.replace(browseRegex.exec(url)[0], '');
    repoUrl = repoUrl.replace("https://globaldevtools.bbva.com","");

    var urlTags = 'https://globaldevtools.bbva.com/bitbucket/rest/api/1.0/projects/' + repository.project.key + '/repos/' + repository.slug + '/tags';
    var country = domainRegex.exec(repoName)[1];
    var newRepoProperties = {
      'display-name': obtainDomainName(repoName),
      'repository-url': repoUrl,
      'tag': urlTags,
      'country': country
    };
    return newRepoProperties;
}

function addedRepo(domains, domainName, groupName, repository) {
    var groupsRegex = /-(\w*)$/;
    _.forEach(domains, function (domain) {
        if (domain.title === domainName) {
            _.forEach(domain.sections, function (section) {
                if (section.name === groupsRegex.exec(groupName)[1]) {
                    section.domains.push(repository)
                }
            });
        }
    });
}

function createStructure(repositories) {
    var domainRegex = /(\w*)-/,
        groupsRegex = /-(\w*)$/,
        localName = /(-)(\w*)(-)/,
        structure = [],
        auxDomains = [],
        auxGroups = [],
        localGroups = [];
    _.forEach(repositories, function (repository) {
      // we remove the prefix GLAPI, ARAPI, CLAPI, etc and the final part of
      // the repo name to be like the old project name
        var domainName = repository.name.replace(domainRegex.exec(repository.name)[0], '');
        var projectName = domainName.split("-");
        var size = projectName[projectName.length-1].length + 1;
        domainName = domainName.slice(0,-size);

        if (domainName.search('Site & Tools') === -1) domainName = domainName.search('global') !== -1 ? domainName : domainName.replace(domainRegex.exec(domainName)[1], 'local');

        if (domainName.search('Site & Tools') === -1 && auxDomains.indexOf(domainName.replace(groupsRegex.exec(domainName)[0], '')) === -1) {
            // TODO: icon
            structure.push({
                "title": domainName.replace(groupsRegex.exec(domainName)[0], ''),
                "sections": [{
                    "name": groupsRegex.exec(domainName)[1],
                    "domains": [trimRepoProperties(repository)]
                }]
            });
            auxDomains.push(domainName.replace(groupsRegex.exec(domainName)[0], ''));
            domainName.search('global') !== -1 ? auxGroups.push(domainName) : localGroups.push(domainName)
        } else if (domainName.search('Site & Tools') === -1 && (domainName.search('global') !== -1 ? auxGroups.indexOf(domainName) === -1 : localGroups.indexOf(domainName) === -1)) {
            _.forEach(structure, function (domain, index) {
                if (domainName.replace(groupsRegex.exec(domainName)[0], '') === domain.title) {
                    // TODO: icon
                    structure[index].sections.push({
                        "name": groupsRegex.exec(domainName)[1],
                        "domains": [trimRepoProperties(repository)]
                    })
                }
            })
            domainName.search('global') !== -1 ? auxGroups.push(domainName) : localGroups.push(domainName)
        } else if (domainName.search('Site & Tools') === -1) {
            addedRepo(structure, domainName.replace(groupsRegex.exec(domainName)[0], ''), domainName, trimRepoProperties(repository));
        }
    });
    return structure;
}

function obtainTags(tagsResponse) {
    let tags = [];
    _.forEach(tagsResponse.values, function(tag) {
        tags.push(tag.displayId);
    });
    return tags;
}

function addedTags(structure) {
    let promises = [];

    _.forEach(structure['domain-groups'], function (domain) {
        _.forEach(domain.sections, function (section) {
            _.forEach(section.domains, function (repo) {
                var deferred = Q.defer();
                requestPromise({
                    url: repo.tag,
                    headers: {
                        'Authorization': 'Basic ' + 'Ym90LWNhdGFsb2ctcmVhZDpHZHRabkVMWA=='
                    },
                    proxy: proxyData
                }).then(function (data) {
                    deferred.resolve(repo.tag = obtainTags(JSON.parse(data)));
                })
                promises.push(deferred.promise)
            });
        });
    });
    Q.all(promises).then(function() {
        fs.outputFileSync('../gen-site/src/structure.json', JSON.stringify(extendStructureJson(structure), null, 4));
        console.log('structure.json updated successfully');
    })
}

function createApiCatalog() {
  var projects = ["ARAPI", "CLAPI", "COAPI", "ESAPI", "GLAPI", "MXAPI", "PEAPI", "PYAPI", "TRAPI", "USAPI", "UYAPI", "VEAPI"]
  var repos = []
  var promises = []
  _.forEach(projects, function (project) {
      var deferred = Q.defer();
      requestPromise({
          url: 'https://globaldevtools.bbva.com/bitbucket/rest/api/1.0/projects/' + project + '/repos?limit=1000',
          headers: {
              'Authorization': 'Basic ' + 'Ym90LWNhdGFsb2ctcmVhZDpHZHRabkVMWA=='
          },
          proxy: proxyData
      }).then(function (data) {
          deferred.resolve(repos = repos.concat(obtainRepos(JSON.parse(data))));
      })
      promises.push(deferred.promise)
  });
  Q.all(promises).then(function() {
    var structure = {}
    structure = {
      "domain-groups": createStructure(repos)
    };
    addedTags(structure);
  }).catch(function(error){
    console.log(error)
  })
}

function isCommonsRepo(repo) {
  // In case you need to generate all repos (including commons), uncomment next line
  // commonsRepos = []
  return commonsRepos.includes(repo.slug)
}

function obtainRepos(data){
  // In case you need to generate all repos (including commons), switch next lines
  // return data.values
  return data.values.filter(repo => !isCommonsRepo(repo))
}

function extendStructureJson(newStructureJson, oldStructureJsonPath, encoding){
  oldStructureJsonPath = oldStructureJsonPath ? oldStructureJsonPath : '../gen-site/src/structure.json'
  encoding = encoding ? encoding : 'utf8'
  var oldJson = JSON.parse(fs.readFileSync(oldStructureJsonPath, encoding));
  return merge(oldJson, newStructureJson);
}

module.exports = {
    createApiCatalog: createApiCatalog
};
