(function () {
    'use strict';

    /** @ngInject */
    function ApiController($scope, ApiService, $location, $anchorScroll) {
        var vm = this,
            displays = [];

        $scope.countries = ['españa', 'chile', 'mexico', 'usa', 'argentina', 'peru', 'paraguay', 'uruguay', 'colombia', 'turquia', 'venezuela']

        vm.apiCatalog = JSON.parse(localStorage.getItem('catalog'));

        if (localStorage.getItem('statusCodes') !== 'undefined') {
            vm.statusCodes = JSON.parse(localStorage.getItem('statusCodes'));
        }

        if (localStorage.getItem('errors') !== 'undefined') {
            vm.functionalErrors = JSON.parse(localStorage.getItem('errors'));
        }



        vm.getTags = function (apiName) {
            vm.tags = [];
            vm.apiName = apiName;
            ApiService.repoTags(apiName)
                .then(function (data) {
                    angular.forEach(data.values, function (tag) {
                        vm.tags.push(tag.displayId);
                    });
                })
        }

        vm.navigateToTag = function () {
          // recuperamos url sin el dominio
          var urlAux = location.hash;
          var res = urlAux.split("/");
          var version = res[res.length-1];
          // comprobar si el ultimo split no es una version
          if( /^[a-z][a-z]*/.test(version)==true) {
            var url = location.hash.replace('#', '') + '/' + vm.selectedVersion;
          } else {
            // url sin la version anterior
            var url2 = urlAux.slice(0, (res[res.length-1].length)*-1);
            var url = url2.replace('#', '') + vm.selectedVersion;
          }
          $location.path(url);
        }

        ApiService.getLocation($location)
            .then(function (data) {
              var jsonFlags = data.data
              var servicesNames = [];
              var countriesPerService = [];
              var differentsFlags = [];
              var servicesNamesMethod = [];
              var countriesPerServiceMethod = [];
              var differentsFlagsMethod = [];
              var auxFlags = [];
              function runThrough() {
                  for (var i in jsonFlags) {
                      servicesNames.push(jsonFlags[i].endpoint.toString());
                      auxFlags = [];
                      var newW = [];
                      jsonFlags[i].methods.forEach(function (el, index, v) {
                          el.implementations.forEach(function (elll, indexx, vv) {
                              if (auxFlags.indexOf(elll.country) === -1) {
                                  if(elll.status === 'R' || elll.status === 'W' || elll.status === 'M'){
                                      newW.push({
                                          country: elll.country,
                                          version: elll.version
                                      })
                                      auxFlags.push(elll.country);
                                  }

                                  if(elll.country === 'gl'){
                                      newW.push({
                                          country: 'global'
                                      })
                                      auxFlags.push(elll.country);
                                  }
                              }
                          });
                      });
                      countriesPerService.push(newW);
                  }
              }

              function formArrayOfJson() {
                  var oo = {};
                  servicesNames.forEach(function (el, index, v) {
                      oo = {}
                      oo[v[index]] = countriesPerService[index];
                      differentsFlags.push(oo);
                  })
              }
              function runThroughMethod() {
                  for (var i in jsonFlags) {
                      servicesNamesMethod.push(jsonFlags[i].endpoint.toString());
                      var newW = [];
                      jsonFlags[i].methods.forEach(function (el, index, v) {
                          el.implementations.forEach(function (elll, indexx, vv) {
                              if(elll.status === 'R' || elll.status === 'W' || elll.status === 'M'){
                                  newW.push({
                                      country: elll.country,
                                      status: elll.status,
                                      method: el.verb.toLowerCase(),
                                      version: elll.version
                                  })
                              }
                              if(elll.country === 'gl'){
                                  newW.push({
                                      country: 'global',
                                      method: el.verb.toLowerCase()
                                  })
                              }
                          });
                      });
                      countriesPerServiceMethod.push(newW);
                  }
              }

              function formArrayOfJsonMethod() {
                  var oo = {};
                  servicesNamesMethod.forEach(function (el, index, v) {
                      oo = {}
                      oo[v[index]] = countriesPerServiceMethod[index];
                      differentsFlagsMethod.push(oo);
                  })
              }
              runThrough();
              formArrayOfJson();
              runThroughMethod();
              formArrayOfJsonMethod();

              vm.flags = differentsFlags
              vm.flagsMethod = differentsFlagsMethod

            });

        vm.showTypeExample = 'object';

        vm.scrollTo = function (id) {
            $location.hash(id);
            $anchorScroll();
        }

        vm.hide = function (id) {
            angular.element(document.getElementById(id)).toggleClass('hide-body');
        }

        vm.showMethod = function ($event, propertie, methodName) {
            var element = angular.element($event.target),
                scope = element.scope();

            scope[propertie] = methodName;
            scope[propertie + 'ActiveButton'] = methodName;
        }

        vm.showDataTypeInfo = function (id) {
            displays = displays.length < 1 ? document.getElementsByClassName('display') : displays;

            angular.forEach(displays, function (display) {
                if (display.id === id) {
                    angular.element(display).addClass('show-display');
                } else {
                    angular.element(display).removeClass('show-display');
                }
            });
        }

        vm.scrollToDataType = function (id) {
            document.getElementById(id).scrollIntoView()
        }
    }

    angular
        .module('passbeta')
        .controller('ApiController', ApiController);
})();
