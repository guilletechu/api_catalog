
(function () {
  'use strict';

  function orderAlpha() {
	  	return function (items, field, reverse) {
			var filtered = [];
			angular.forEach(items, function (item) {
				filtered.push(item);
			});
			filtered.sort(function (a, b) {
				return (a[field] > b[field] ? 1 : -1);
			});
			if (reverse) filtered.reverse();
			return filtered;
  		};
	}

	function filterByCountry() {
		return function (array, expression) {
			var results = [];

			if (!expression && expression !== 0 || expression === '') {
				return array;
			}

			angular.forEach(array, function(repository) {
				if (repository.country.search(expression) !== -1) {
					results.push(repository);
				}
			})
			return results;
		}

	}

  angular
    .module('passbeta')
    .filter('orderAlpha', orderAlpha)
    .filter('filterByCountry', filterByCountry);

})();