(function() {
  'use strict';

  /** @ngInject */
  function HomeService($http, $q) {
    function getNews() {
      var deferred = $q.defer();
      $http({
            method: "GET",
            url: '/bitbucket/projects/BPC/repos/apisbbva.bitbucket.org/raw/activities.json'
        }).then(function(data) {
            deferred.resolve(data.data);
        });
        return deferred.promise;
    }

    return {
      getNews: getNews
    }
  }

  angular
    .module('passbeta')
    .factory('HomeService', HomeService);
})();
