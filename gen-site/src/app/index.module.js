(function(angular) {
  'use strict';
  angular
    .module('passbeta', ['ngAnimate', 'ngTouch', 'ngSanitize', 'ngRoute', 'ngMaterial', 'ngCookies', 'jsonFormatter']);
})(this.angular);
