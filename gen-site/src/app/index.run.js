(function () {
  'use strict';
  /** @ngInject */
  function runBlock(Structure, $templateCache) {
    angular.forEach(Structure, function (value, key) {
      $templateCache.put(key, value);
    })
  }
  angular
    .module('passbeta')
    .run(runBlock);
})();
