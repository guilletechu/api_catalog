(function() {
  'use strict';

  /** @ngInject */
  function ApiConsumerController($location, $anchorScroll) {
    var vm = this;
    vm.scrollTo = function(id) {
      $location.hash(id);
      $anchorScroll();
    }
  }
  angular
    .module('passbeta')
    .controller('ApiConsumerController', ApiConsumerController);
})();
