var fs = require('fs'),
    Q = require('q'),
    _ = require('lodash');

function readDoc(path, file){
  var deferred = Q.defer();
  fs.readFile(path+'docs/'+file, 'utf8', function(err, data) {
    if (err) deferred.reject(err);
    var obj = {};
    obj.file = file;
    obj.path = path;
    obj.data = data;
    deferred.resolve(obj);
  });
  return deferred.promise;
}

function generateDataType(object){
  var data = object.data;
  var match;
  var regExr = /\|(.+)\|(.+)\|(.+)\|(.+)\|/g;
  regExr.exec(data);
  regExr.exec(data);
  var resultData = {};
  resultData.sub = [];
  while(match = regExr.exec(data)){
    var obj = {};
    var name;
    var arr = match[1].split('.');
    var index = resultData.sub;
    if(arr.length === 1){
      name = arr[0];
    } else {
      arr.slice(1).forEach(function(elem){
        if(!index[index.length-1].sub){
          index[index.length-1].sub = [];
        }
        index = index[index.length-1].sub;
        name = elem;
      });
    }
    obj.name = name;
    if(match[2] === 'Timestamp'){
      obj.type = 'date';
    } else {
      obj.type = match[2].toLowerCase();
    }
    if(match[3] === 'Optional' || match[3] === 'optional'){
      obj.required = false;
    } else {
      obj.required = true;
    }
    obj.description = match[4];
    index.push(obj);
  }
  return generateRAML(resultData, object);
}

function generateRAML(resultData, object){
  if (!fs.existsSync(object.path + 'types/')) {
    fs.mkdirSync(object.path + 'types/');
  }

  var indexTab = 2;
  var json = resultData.sub;

  function writeString(json){
    var result = '';
    for(var i = 0; i <= indexTab; i++){
      result += '  ';
    }
    result += json.name+':\n';
    indexTab++;
    for(var i = 0; i <= indexTab; i++){
      result += '  ';
    }
    result += 'type: ' + json.type + '\n';
    for(var i = 0; i <= indexTab; i++){
      result += '  ';
    }
    result += 'description: |\n';
    indexTab++;
    for(var i = 0; i <= indexTab; i++){
      result += '  ';
    }
    indexTab--;
    result += json.description + '\n';
    if(json.sub){
      for(var i = 0; i <= indexTab; i++){
        result += '  ';
      }
      indexTab++;
      if(json.type === 'array'){
        result += 'items:\n';
        for(var i = 0; i <= indexTab; i++){
          result += '  ';
        }
        indexTab++;
      }
      result += 'properties:\n';
      json.sub.forEach(function(elem){
        result += writeString(elem);
      });
      if(json.type === 'array'){
        indexTab--;
      }
      indexTab--;
    }
    indexTab--;
    return result;
  }

  if(json.length > 0){
    var wstream = fs.createWriteStream(object.path + 'types/' + object.file.substring(0, object.file.lastIndexOf('.')) + '.raml');
    wstream.write('#%RAML 1.0 DataType\n');
    wstream.write('properties:\n');
    wstream.write('  data:\n');
    wstream.write('    properties:\n');
    var result = '';
    json.forEach(function(elem){
      result += writeString(elem);
    });
    wstream.write(result);
    wstream.end();
  }
}

function generate(path){
  fs.readdir(path + 'docs/', function(err, files) {
    if (err) throw err;
    files.forEach(function(file){
      readDoc(path, file)
        .then(generateDataType);
    });
  });
}

module.exports = {
  generate:generate
};
