## The new version of API accounts 0.3.0 is available. 

See below for bugs solved, user histories and version details.

### Bug

*   [[GABI1-376](https://globaldevtools.bbva.com/jira/browse/GABI1-376)] - Revisión de todos los arrays en el API de accounts
*   [[GABI1-430](https://globaldevtools.bbva.com/jira/browse/GABI1-430)] - Inclusión de nuevos filtros para /accounts
*   [[GABI1-443](https://globaldevtools.bbva.com/jira/browse/GABI1-443)] - Revisar los arrays del API de accounts
*   [[GABI1-481](https://globaldevtools.bbva.com/jira/browse/GABI1-481)] - queryParam erróneo como required en accounts
*   [[GABI1-482](https://globaldevtools.bbva.com/jira/browse/GABI1-482)] - Añadir queryParams de paginación

### Story

*   [[GABI1-126](https://globaldevtools.bbva.com/jira/browse/GABI1-126)] - Definición y uso de Paginación en Accounts
*   [[GABI1-138](https://globaldevtools.bbva.com/jira/browse/GABI1-138)] - Mostrar si la cuenta SPEI está asociada a un móvil
*   [[GABI1-191](https://globaldevtools.bbva.com/jira/browse/GABI1-191)] - Añadir nuevos tipos de cuenta
*   [[GABI1-196](https://globaldevtools.bbva.com/jira/browse/GABI1-196)] - Filtrar cuentas por indicador de destino de un traspaso
*   [[GABI1-258](https://globaldevtools.bbva.com/jira/browse/GABI1-258)] - Indicadores comportamiento en cuenta para traspaso tarjeta-cuenta
*   [[GABI1-310](https://globaldevtools.bbva.com/jira/browse/GABI1-310)] - Habilitar "Número de Cuenta Simple"
*   [[GABI1-328](https://globaldevtools.bbva.com/jira/browse/GABI1-328)] - Comportamiento cuentas para hacer transferencias
*   [[GABI1-339](https://globaldevtools.bbva.com/jira/browse/GABI1-339)] - Documentación de enumerados en Accounts
*   [[GABI1-385](https://globaldevtools.bbva.com/jira/browse/GABI1-385)] - Migración nueva candidate release RAML ACCOUNTS
*   [[GABI1-436](https://globaldevtools.bbva.com/jira/browse/GABI1-436)] - Producto asociado a una cuenta
*   [[GABI1-438](https://globaldevtools.bbva.com/jira/browse/GABI1-438)] - Descripciones en Accounts
*   [[GABI1-448](https://globaldevtools.bbva.com/jira/browse/GABI1-448)] - Creación endpoint participants
*   [[GABI1-466](https://globaldevtools.bbva.com/jira/browse/GABI1-466)] - Añadir atributos para tipos de cuenta crédito
*   [[GABI1-480](https://globaldevtools.bbva.com/jira/browse/GABI1-480)] - Indicadores para solicitar moneda extranjera y para pagar servicios
*   [[GABI1-503](https://globaldevtools.bbva.com/jira/browse/GABI1-503)] - Alta de Cuenta ( Cuenta Digital )


## CHANGELOG


### Bug Fixes

* **accounts:** Adapt to the new RAML version
* **accounts:** Added endpoint related-contracts and trait expandable
* **accounts:** Added full view
* **accounts:** Added new queryParameters
* **accounts:** Added queryParameters for pagination
* **accounts:** Added the atrtibutes to credit accounts. GABI1-466
* **accounts:** Change date
* **accounts:** Correct datetime format
* **api.raml:** Added the filters to the list of /accounts and /accounts/id/transactions
* **api.raml:** Added the new filter to the list of GET /accounts
* **example:** Added reminderCode to expands example
* **example:** Changed accountType to a valid one.
* **examples:** Added multiexample to GET /accounts
* **examples:** Changed titular to holder in participants examples.
* **examples:** Changes to make examples match each other.
* **examples:** Fixed some errors in pagination examples.
* **indicators:** Added new indicator ASSOCIABLE_TO_PHONE_FOR_SPEI
* **indicators:** Fixed indicators list with some lost modifications.
* **participants:** Changed participant description to match what is already defined in account-id
* **raml:** A new numberType.id is added
* **raml:** Added a new indicator
* **raml:** Added indicators of GABI1-328
* **raml:** Change balance for balances in arrays
* **raml:** Change indicator in api.raml
* **raml:** Change indicators due Glomo specs
* **raml:** Changes due GABI1-258. New indicators are added
* **raml:** Changes for BUG GABI1-376
* **raml:** Example for filter added
* **raml:** Fixed little details in JSON example
* **raml:** Fixed queryparam as non required
* **raml:** Indentation errors.
* **raml:** Resolved conflicts with api.raml
* **raml:** Fixed some bugs
* **related-contract:** Added product.id in PATCH method
* **related-contracts:** Changed description in header

### Features

* **accounts:** Endpoint /participants added
* **api.raml:** Added the new filters to GET /accounts. GABI1-466
* **raml:** New description for SIMPLE_ACCOUNT enum has been added
* **raml:** Added a new description for SIMPLE_ACCOUNT
* **raml:** Added two new indicators
* **raml:** Enum descriptions are now added. Not finally descriptions
* **raml:** Added indicatorId as a new queryParam
* **types:** Added the new values to the enum "accountType.id". GABI1-191
