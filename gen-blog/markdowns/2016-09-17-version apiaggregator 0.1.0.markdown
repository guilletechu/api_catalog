## The new API api-aggregator 0.1.0 is available.

This API includes one service that manage several API requests.

<br>

See below for bugs solved, user histories and version details.

### Feature

*   [[GABI1-542](https://globaldevtools.bbva.com/jira/browse/GABI1-542)] - Servicio API Aggregator

## CHANGELOG

### Bug Fixes

* **api:** initial commit ([8a50921](https://bitbucket.org/apisbbva/global-apis-technical-apiaggregator-v0.git/commits/8a50921))
* **api:** review ([67367fc](https://bitbucket.org/apisbbva/global-apis-technical-apiaggregator-v0.git/commits/67367fc))
* **description:** change description of api ([15ea198](https://bitbucket.org/apisbbva/global-apis-technical-apiaggregator-v0.git/commits/15ea198))
* **error:** 207 ([e48f68f](https://bitbucket.org/apisbbva/global-apis-technical-apiaggregator-v0.git/commits/e48f68f))
* **html:** add view ([43f0a9f](https://bitbucket.org/apisbbva/global-apis-technical-apiaggregator-v0.git/commits/43f0a9f))
* **rename:** api name ([8c2944e](https://bitbucket.org/apisbbva/global-apis-technical-apiaggregator-v0.git/commits/8c2944e))
* **view:** change view name ([ffc28fd](https://bitbucket.org/apisbbva/global-apis-technical-apiaggregator-v0.git/commits/ffc28fd))

### Features

* **repo:** initial commit ([bf0b21d](https://bitbucket.org/apisbbva/global-apis-technical-apiaggregator-v0.git/commits/bf0b21d))
