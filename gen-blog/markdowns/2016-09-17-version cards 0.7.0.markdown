## The new version of API cards 0.7.0 is available.

### Changes in GET /balances
* Add filter balances by the type of card.

<br>

See below for bugs solved, user histories and version details.

### Feature

*   [[GABI1-1242](https://globaldevtools.bbva.com/jira/browse/GABI1-1242)] - Agregar filtro a los balances por el tipo de tarjeta

## CHANGELOG

### Bug Fixes

* **cards:** remove description ([7780555](https://bitbucket.org/apisbbva/global-apis-products-cards-v0.git/commits/7780555))
* **filter:** add balance filter ([ffc906a](https://bitbucket.org/apisbbva/global-apis-products-cards-v0.git/commits/ffc906a))
* **rever:** description ([ce4d4fa](https://bitbucket.org/apisbbva/global-apis-products-cards-v0.git/commits/ce4d4fa))
