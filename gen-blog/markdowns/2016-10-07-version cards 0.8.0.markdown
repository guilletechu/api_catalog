## The new version of API cards 0.8.0 is available.

### Changes in:
* GET /cards/{card-id}/related-contracts
* POST /cards/{card-id}/related-contracts
* PATCH /cards/{card-id}/related-contracts/{related-contract-id}

<br>

See below for bugs solved, user histories and version details.

### Feature

*   [[GABI1-1070](https://globaldevtools.bbva.com/jira/browse/GABI1-1070)] - Establecer cuenta SPEI

## CHANGELOG

### Bug Fixes

* **currency:** delete type currency ([b249571](https://bitbucket.org/apisbbva/global-apis-products-cards-v0.git/commits/b249571))
* **put:** http 201 created ([4848622](https://bitbucket.org/apisbbva/global-apis-products-cards-v0.git/commits/4848622))
