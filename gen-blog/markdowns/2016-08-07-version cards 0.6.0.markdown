## The new version of API cards 0.6.0 is available. 

See below for bugs solved, user histories and version details.

### Story

* [[GABI1-668](https://globaldevtools.bbva.com/jira/browse/GABI1-668)] - Descripción de grantedCredits incorrecta
- [[GABI1-507](https://globaldevtools.bbva.com/jira/browse/GABI1-507)] - Extracción de Types a commons CARDS
+ [[GABI1-548](https://globaldevtools.bbva.com/jira/browse/GABI1-548)] - Mejora de descripción de availableBalance para tarjetas de débito en listado
+ [[GABI1-549](https://globaldevtools.bbva.com/jira/browse/GABI1-549)] - Mejora de descripción de availableBalance para tarjetas de débito en detalle
+ [[GABI1-551](https://globaldevtools.bbva.com/jira/browse/GABI1-551)] - Nuevos indicadores para pago de servicios y recepciones internacionales
+ [[GABI1-553](https://globaldevtools.bbva.com/jira/browse/GABI1-553)] - Motivos de alta y cancelación de tarjeta
+ [[GABI1-555](https://globaldevtools.bbva.com/jira/browse/GABI1-555)] - Filtrado de movimientos por divisa
+ [[GABI1-613](https://globaldevtools.bbva.com/jira/browse/GABI1-613)] - Añadir paginación a los listados
+ [[GABI1-663](https://globaldevtools.bbva.com/jira/browse/GABI1-663)] - Incluir gasto realizado a la fecha de corte


## CHANGELOG

### Bug Fixes

* **api:** Change include type ([27abdef](https://bitbucket.org/apisbbva/global-apis-products-cards-v0.git/commits/27abdef))
* **api:** Changes due to validation ([ac579fa](https://bitbucket.org/apisbbva/global-apis-products-cards-v0.git/commits/ac579fa))
* **cards:** add expands ([1a6d396](https://bitbucket.org/apisbbva/global-apis-products-cards-v0.git/commits/1a6d396))
* **cards:** add pagination and expands ([7cae897](https://bitbucket.org/apisbbva/global-apis-products-cards-v0.git/commits/7cae897))
* **cards:** delete card block ([c7b1895](https://bitbucket.org/apisbbva/global-apis-products-cards-v0.git/commits/c7b1895))
* **cards:** delete related contract from resource ([eca5f5a](https://bitbucket.org/apisbbva/global-apis-products-cards-v0.git/commits/eca5f5a))
* **cards:** remove expands pin ([bf9f2db](https://bitbucket.org/apisbbva/global-apis-products-cards-v0.git/commits/bf9f2db))
* **cards:** undoing related contract for patch a post ([0c50ed3](https://bitbucket.org/apisbbva/global-apis-products-cards-v0.git/commits/0c50ed3))
* **descriptions:** Changed availableBalance descriptions to be more accurate for each card type. ([ccd2b57](https://bitbucket.org/apisbbva/global-apis-products-cards-v0.git/commits/ccd2b57))
* **indicators:** Fixed some indicators descriptions and the examples where indicators are retriev ([9a357f9](https://bitbucket.org/apisbbva/global-apis-products-cards-v0.git/commits/9a357f9))

### Features

* Corrected the errors of HQ. GABI1-551 ([ec8f226](https://bitbucket.org/apisbbva/global-apis-products-cards-v0.git/commits/ec8f226))
* Corrected the errors of HQ. GABI1-551 ([dc6106f](https://bitbucket.org/apisbbva/global-apis-products-cards-v0.git/commits/dc6106f))
* Extract types to commons. GABI1-507 ([2cdc18e](https://bitbucket.org/apisbbva/global-apis-products-cards-v0.git/commits/2cdc18e))
* **paymentMethod:** Added LAST_PERIOD_AMOUNT to paymentAmounts type GABI1-663 ([328475d](https://bitbucket.org/apisbbva/global-apis-products-cards-v0.git/commits/328475d))
* **relatedContracts:** Added relationTypes values for tracking card substitutions. ([2431d99](https://bitbucket.org/apisbbva/global-apis-products-cards-v0.git/commits/2431d99))
* **versions:** CL versions ([a24d6ee](https://bitbucket.org/apisbbva/global-apis-products-cards-v0.git/commits/a24d6ee))

