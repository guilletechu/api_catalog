## The new version of API catalogs 0.2.0 is available.

An API to get global catalogs info.

### New methods
* GET /catalogs/{catalog-id}/records


### Modified methods
* GET /catalogs


See below for bugs solved, user histories and version details.

### Story

*   [[GABI1-1391](https://globaldevtools.bbva.com/jira/browse/GABI1-1391)] - Modificaciones al servicio que obtiene los catálogos y nuevo servicio para obtener la información de un catálogo

## CHANGELOG

### Bug Fixes

* **catalogs:** Changed catalog structure to allow pagination in catalog list. ([7a29d1a](https://bitbucket.org/apisbbva/global-apis-data-catalogs-v0.git/commits/7a29d1a))
* **enhancedCatalogs:** Removed enhanced catalog definition. ([6f979ec](https://bitbucket.org/apisbbva/global-apis-data-catalogs-v0.git/commits/6f979ec))
* **raml:** add filter by id ([5b95a23](https://bitbucket.org/apisbbva/global-apis-data-catalogs-v0.git/commits/5b95a23))
* **raml:** add filter by id ([19843be](https://bitbucket.org/apisbbva/global-apis-data-catalogs-v0.git/commits/19843be))
* **raml:** delete file ([a7403ee](https://bitbucket.org/apisbbva/global-apis-data-catalogs-v0.git/commits/a7403ee))

### Features

* **api:** Added name as queryParam in GET /catalogs ([9a7e6d9](https://bitbucket.org/apisbbva/global-apis-data-catalogs-v0.git/commits/9a7e6d9))
* **commonLists:** Added common-list type and record subtype ([7ae4496](https://bitbucket.org/apisbbva/global-apis-data-catalogs-v0.git/commits/7ae4496))
* **description:** Improved description about what a catalog is and its aim. ([953f922](https://bitbucket.org/apisbbva/global-apis-data-catalogs-v0.git/commits/953f922))
* **enhancedcatalogs:** Added enhanced catalogs ([732b160](https://bitbucket.org/apisbbva/global-apis-data-catalogs-v0.git/commits/732b160))
* **examples:** Changed IDs to English. ([8d6b53a](https://bitbucket.org/apisbbva/global-apis-data-catalogs-v0.git/commits/8d6b53a))
* **ids:** Renamed catalogId and recordId to just id. Removed typeValue.raml (no longer nee ([dd2f494](https://bitbucket.org/apisbbva/global-apis-data-catalogs-v0.git/commits/dd2f494))
