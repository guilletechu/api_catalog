## The new API geographic-places 0.1.0 is available.

This API allows the consumption of geographic data based on different axes: states, cities or specific urban locations like districts or neighborhoods.

### New methods
* GET /states
* GET /states/{state-id}
* GET /cities
* GET /cities/{city-id}
* GET /places
* GET /places/{place-id}
<br>

See below for bugs solved, user histories and version details.

### Feature

* [[GABI1-1085](https://globaldevtools.bbva.com/jira/browse/GABI1-1085)] - API de geographic-places para poder obtener la dirección en base a un código postal

## CHANGELOG



### Bug Fixes

* **raml:** Enums for geographicGroupType and json examples added. ([94a635f](https://bitbucket.org/apisbbva/global-apis-data-geographicplaces-v0.git/commits/94a635f))
* **raml:** Pull request's observations for geographic places GABI1-1185 (description and ex ([0b26f1e](https://bitbucket.org/apisbbva/global-apis-data-geographicplaces-v0.git/commits/0b26f1e))

### Features

* **all:** First version for geographic places GABI1-1253 ([9c36b35](https://bitbucket.org/apisbbva/global-apis-data-geographicplaces-v0.git/commits/9c36b35))
* **all:** Refactoring post pair review for geographic places GABI1-1185 ([2b66c25](https://bitbucket.org/apisbbva/global-apis-data-geographicplaces-v0.git/commits/2b66c25))
* **all:** Refactoring post pair review for geographic places GABI1-1185 ([f42def5](https://bitbucket.org/apisbbva/global-apis-data-geographicplaces-v0.git/commits/f42def5))
* **all:** Refactoring post pair review for geographic places GABI1-1185 ([760bde7](https://bitbucket.org/apisbbva/global-apis-data-geographicplaces-v0.git/commits/760bde7))
* **all:** Refactoring post pair review for geographic places GABI1-1185 ([28aed1b](https://bitbucket.org/apisbbva/global-apis-data-geographicplaces-v0.git/commits/28aed1b))
* **all:** Refactoring post pair review for geographic places GABI1-1185 ([2b6c1a5](https://bitbucket.org/apisbbva/global-apis-data-geographicplaces-v0.git/commits/2b6c1a5))
* **all:** Rename zipcodes by postalcodes for geographic places GABI1-1185 ([1788a78](https://bitbucket.org/apisbbva/global-apis-data-geographicplaces-v0.git/commits/1788a78))
* **all:** Rename zipcodes by postalcodes for geographic places GABI1-1185 ([72ee8ba](https://bitbucket.org/apisbbva/global-apis-data-geographicplaces-v0.git/commits/72ee8ba))
* **all:** Rename zipcodes by postalcodes for geographic places GABI1-1185 ([5441983](https://bitbucket.org/apisbbva/global-apis-data-geographicplaces-v0.git/commits/5441983))
* **all:** Suggestions after team review session for geographic places GABI1-1185 (/localit ([cec4b60](https://bitbucket.org/apisbbva/global-apis-data-geographicplaces-v0.git/commits/cec4b60))
* **all:** Suggestions after team review session for geographic places GABI1-1185 (/places  ([e3f95b6](https://bitbucket.org/apisbbva/global-apis-data-geographicplaces-v0.git/commits/e3f95b6))
* **all:** Suggestions after team review session for geographic places GABI1-1185 (/states: ([53393ca](https://bitbucket.org/apisbbva/global-apis-data-geographicplaces-v0.git/commits/53393ca))
* **all:** Suggestions after team review session for geographic places GABI1-1185 (ISO 3166 ([608bbb6](https://bitbucket.org/apisbbva/global-apis-data-geographicplaces-v0.git/commits/608bbb6))
* **api:** Initial Upload ([5dff9fe](https://bitbucket.org/apisbbva/global-apis-data-geographicplaces-v0.git/commits/5dff9fe))
