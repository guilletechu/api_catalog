## The new version of API accounts 0.10.0 is available.

### New methods added
* DELETE /accounts/{account-id}/customized-formats/{customized-format-id}
* GET /accounts/{account-id}/activations
* GET /accounts/{account-id}/activations/{activation-id}
* PATCH /accounts/{account-id}/activations/{activation-id}
<br>

### Modified methods
* GET /accounts
* GET /accounts/{account-id}
* GET /balances
* GET /cards/{card-id}/blocks/{block-id}
* PATCH /accounts/{account-id}

See below for bugs solved, user histories and version details.

### Feature

*   [[GABI1-1292](https://globaldevtools.bbva.com/jira/browse/GABI1-1292)] - Increase or improve some attributes related to account entity
*   [[GABI1-1338](https://globaldevtools.bbva.com/jira/browse/GABI1-1338)] - Cambios de los filtros
*   [[GABI1-1368](https://globaldevtools.bbva.com/jira/browse/GABI1-1368)] - Increase statuses from the accounts lifecycle
*   [[GABI1-1369](https://globaldevtools.bbva.com/jira/browse/GABI1-1369)] - Delete or close accounts
*   [[GABI1-1370](https://globaldevtools.bbva.com/jira/browse/GABI1-1370)] - Add endpoint to managing account's activations

## CHANGELOG

### Bug Fixes

* **raml:** error view participants ([f7b5490](https://bitbucket.org/apisbbva/global-apis-products-accounts-v0.git/commits/f7b5490))

### Features

* **raml:** new endpoint payment methods ([e6e6061](https://bitbucket.org/apisbbva/global-apis-products-accounts-v0.git/commits/e6e6061))
