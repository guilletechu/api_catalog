## The new version of API deposits 0.1.0 is available.

An API to manage the deposits info of a user: lists, details, related contracts, indicators, participants, conditions and deposits balances.

See below for bugs solved, user histories and version details.

### Feature

*   [[GABI1-732](https://globaldevtools.bbva.com/jira/browse/GABI1-732)] - Modificación del alias de un depósito
*   [[GABI1-735](https://globaldevtools.bbva.com/jira/browse/GABI1-735)] - Consulta listado de depósitos
*   [[GABI1-739](https://globaldevtools.bbva.com/jira/browse/GABI1-739)] - Consulta detalle de un depósito
*   [[GABI1-743](https://globaldevtools.bbva.com/jira/browse/GABI1-743)] - Consulta de los contratos relacionados de un depósito
*   [[GABI1-746](https://globaldevtools.bbva.com/jira/browse/GABI1-746)] - Consulta de indicadores de un depósito
*   [[GABI1-749](https://globaldevtools.bbva.com/jira/browse/GABI1-749)] - Consulta de los importes agregados de depósitos
*   [[GABI1-752](https://globaldevtools.bbva.com/jira/browse/GABI1-752)] - Consulta de los participantes de un depósito
*   [[GABI1-755](https://globaldevtools.bbva.com/jira/browse/GABI1-755)] - Consulta de las condiciones de un depósito

## CHANGELOG

### Bug Fixes

* **api:** add related-contract type ([de2d905](https://bitbucket.org/apisbbva/global-apis-products-deposits-v0.git/commits/de2d905))
* **refactor:** change structure of yields and conditions and add examples ([7640050](https://bitbucket.org/apisbbva/global-apis-products-deposits-v0.git/commits/7640050))
* **refactor:** rename types and add descriptions ([05829e7](https://bitbucket.org/apisbbva/global-apis-products-deposits-v0.git/commits/05829e7))
* **refactor:** rename types and add descriptions ([f77a5ae](https://bitbucket.org/apisbbva/global-apis-products-deposits-v0.git/commits/f77a5ae))
* **refactor:** rename types and add descriptions ([9eb86d4](https://bitbucket.org/apisbbva/global-apis-products-deposits-v0.git/commits/9eb86d4))
* **refactor:** rename types and add descriptions ([c8e60d1](https://bitbucket.org/apisbbva/global-apis-products-deposits-v0.git/commits/c8e60d1))
* **refactor:** rename types and add descriptions ([9312f91](https://bitbucket.org/apisbbva/global-apis-products-deposits-v0.git/commits/9312f91))
* **refactor:** rename types and add descriptions ([7941ebb](https://bitbucket.org/apisbbva/global-apis-products-deposits-v0.git/commits/7941ebb))
* **rename:** rename queryParam expand ([c751b4f](https://bitbucket.org/apisbbva/global-apis-products-deposits-v0.git/commits/c751b4f))
* **traits:** add traits ([8dc71ed](https://bitbucket.org/apisbbva/global-apis-products-deposits-v0.git/commits/8dc71ed))

### Features

* **deposits:** varios issues of deposits ([5090eff](https://bitbucket.org/apisbbva/global-apis-products-deposits-v0.git/commits/5090eff))
