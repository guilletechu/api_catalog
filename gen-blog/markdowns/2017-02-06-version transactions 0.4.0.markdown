## The new version of API transactions 0.4.0 is available.

This API includes any service related to transactions on financial products (i.e. cards or accounts).

### Modified methods
* GET /transactions


See below for bugs solved, user histories and version details.

### Story

* [[GABI1-1440](https://globaldevtools.bbva.com/jira/browse/GABI1-1440)] - Listado de movimientos

## CHANGELOG

### Bug Fixes

* **cash_income:** Fixed indentation on api.raml file. ([8811128](https://bitbucket.org/apisbbva/global-apis-products-transactions-v0.git/commits/8811128))
* **raml:** number format US and pagination ([7a8ebcb](https://bitbucket.org/apisbbva/global-apis-products-transactions-v0.git/commits/7a8ebcb))
* **transaction:** fixed description for toLocalAmount filter. ([3ea79ae](https://bitbucket.org/apisbbva/global-apis-products-transactions-v0.git/commits/3ea79ae))
* **transaction:** Updated description for operationDate and localAmount filters. ([c1b9ef5](https://bitbucket.org/apisbbva/global-apis-products-transactions-v0.git/commits/c1b9ef5))
* **transaction:** Updated operationDate and localAmount.amount filters (back to from&to criteria) ([1c1393a](https://bitbucket.org/apisbbva/global-apis-products-transactions-v0.git/commits/1c1393a))
* **transaction:** Updated operationDate and localAmount.amount filters (back to from&to criteria) ([7aafb6d](https://bitbucket.org/apisbbva/global-apis-products-transactions-v0.git/commits/7aafb6d))
* **transactions:** added filters and order by criteria. ([0c99395](https://bitbucket.org/apisbbva/global-apis-products-transactions-v0.git/commits/0c99395))
* **transactions:** changed details attribute to OPTIONAL on transaction and transactions types. ([28624bc](https://bitbucket.org/apisbbva/global-apis-products-transactions-v0.git/commits/28624bc))
* **transactions:** deleted Content-Location header on all endpoints. ([801d1f4](https://bitbucket.org/apisbbva/global-apis-products-transactions-v0.git/commits/801d1f4))
* **transactions:** deleted flags on transactionType attribute. ([38d2ee2](https://bitbucket.org/apisbbva/global-apis-products-transactions-v0.git/commits/38d2ee2))
* **transactions:** fixed error that does´t allow to show correctly contractNumberType type. ([db917ab](https://bitbucket.org/apisbbva/global-apis-products-transactions-v0.git/commits/db917ab))
* **transactions:** Fixed transactionType errors on examples. ([438bd18](https://bitbucket.org/apisbbva/global-apis-products-transactions-v0.git/commits/438bd18))
* **transactions:** Fixed transactionType.name indentation to prevent a validation error in ATOM. ([0291564](https://bitbucket.org/apisbbva/global-apis-products-transactions-v0.git/commits/0291564))
* **transactions:** updated descriptions in order to match american grammar. ([b4c9a7d](https://bitbucket.org/apisbbva/global-apis-products-transactions-v0.git/commits/b4c9a7d))
* **transactions:** updated enum for financyngType in order to match american grammar. ([68ebbee](https://bitbucket.org/apisbbva/global-apis-products-transactions-v0.git/commits/68ebbee))
* **transactions:** updated filter description. ([9a056dc](https://bitbucket.org/apisbbva/global-apis-products-transactions-v0.git/commits/9a056dc))

### Features

* **cash_withdrawals:** Updated net type and atm type. ([52fd333](https://bitbucket.org/apisbbva/global-apis-products-transactions-v0.git/commits/52fd333))
* **filter:** movement status GABI1-133 ([9e79f62](https://bitbucket.org/apisbbva/global-apis-products-transactions-v0.git/commits/9e79f62))
* **purchases:** Updated payment channels (descriptions are needed) ([c1a85ed](https://bitbucket.org/apisbbva/global-apis-products-transactions-v0.git/commits/c1a85ed))
* **purchases:** Updated purchase type ([6daa0c2](https://bitbucket.org/apisbbva/global-apis-products-transactions-v0.git/commits/6daa0c2))
* **transactions:** Added cash income details endpoint and its examples. ([ecde68c](https://bitbucket.org/apisbbva/global-apis-products-transactions-v0.git/commits/ecde68c))
* **transactions:** Added cash withdrawal details endpoint and its examples. ([672eebe](https://bitbucket.org/apisbbva/global-apis-products-transactions-v0.git/commits/672eebe))
* **transactions:** Added concept filter criteria. ([ea4fcf2](https://bitbucket.org/apisbbva/global-apis-products-transactions-v0.git/commits/ea4fcf2))
* **transactions:** Added filters for localAmount.amount and transactionType.internalCode. ([8156a4c](https://bitbucket.org/apisbbva/global-apis-products-transactions-v0.git/commits/8156a4c))
* **transactions:** added moneyFlow attribute to transactions a transaction types. ([1e1e610](https://bitbucket.org/apisbbva/global-apis-products-transactions-v0.git/commits/1e1e610))
* **transactions:** Added purchases details endpoint and it example. ([b7c20fb](https://bitbucket.org/apisbbva/global-apis-products-transactions-v0.git/commits/b7c20fb))
* **transactions:** Added refund details endpoint and its example. ([d645e23](https://bitbucket.org/apisbbva/global-apis-products-transactions-v0.git/commits/d645e23))
* **transactions:** Modified types for GNC purposes. ([3e4c7fd](https://bitbucket.org/apisbbva/global-apis-products-transactions-v0.git/commits/3e4c7fd))
* **transactions:** Updated query parameters for transactions GET operation. ([33b64ab](https://bitbucket.org/apisbbva/global-apis-products-transactions-v0.git/commits/33b64ab))
* **transactions:** Updated transactions and transaction types. ([f776bd6](https://bitbucket.org/apisbbva/global-apis-products-transactions-v0.git/commits/f776bd6))
