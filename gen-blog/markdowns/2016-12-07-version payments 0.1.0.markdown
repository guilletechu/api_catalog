## The new API Payments 0.1.0 is available.

This API offers a collection of resources and methods to perform useful operations related to the payment of services.

### New methods
* GET /services
* GET /services/{service-id}
* GET /services/{service-id}/additional-payment-fields
* GET /services/{service-id}/additional-payment-fields/{additional-payment-field-id}
* GET /services/{service-id}/bills
* GET /stored-services
* POST /stored-services
* GET /stored-services/{store-service-id}
* PATCH /stored-services/{store-service-id}
* DELETE /stored-services/{store-service-id}
* GET /service-payments
* POST /service-payments
* GET /service-types
* GET /service-types/{service-type-id}
<br>

See below for bugs solved, user histories and version details.

### Feature

* [[GABI1-921](https://globaldevtools.bbva.com/jira/browse/GABI1-921)] - Pago de un servicio para el pago de servicios
* [[GABI1-926](https://globaldevtools.bbva.com/jira/browse/GABI1-926)] - Operaciones con los servicios frecuentes para el pago de servicios
* [[GABI1-930](https://globaldevtools.bbva.com/jira/browse/GABI1-930)] - Operaciones con los servicios disponibles para el pago de servicios

## CHANGELOG


### Bug Fixes

* **billId:** add rename billId to id ([870897c](https://bitbucket.org/apisbbva/global-apis-paymentmethods-payments-v0.git/commits/870897c))
* **delete:** simulated ([397c416](https://bitbucket.org/apisbbva/global-apis-paymentmethods-payments-v0.git/commits/397c416))
* **descriptions:** add resources links ([2b04747](https://bitbucket.org/apisbbva/global-apis-paymentmethods-payments-v0.git/commits/2b04747))
* **examples:** Fixed typos in examples of links ([f66723c](https://bitbucket.org/apisbbva/global-apis-paymentmethods-payments-v0.git/commits/f66723c))
* **payments:** Changed type to highRanked field to boolean. ([cdf9a65](https://bitbucket.org/apisbbva/global-apis-paymentmethods-payments-v0.git/commits/cdf9a65))
* **payments:** Updated descriptions and resources relations on api.raml and types folder. ([516d79c](https://bitbucket.org/apisbbva/global-apis-paymentmethods-payments-v0.git/commits/516d79c))
* **raml:** restore isSimulated ([42f14f7](https://bitbucket.org/apisbbva/global-apis-paymentmethods-payments-v0.git/commits/42f14f7))
* **references:** Added lost references to types ([8d233de](https://bitbucket.org/apisbbva/global-apis-paymentmethods-payments-v0.git/commits/8d233de))

### Features

* **payments:** Added endpoints and methods for service payments. ([4d4e348](https://bitbucket.org/apisbbva/global-apis-paymentmethods-payments-v0.git/commits/4d4e348))
* **payments:** Added example files for all operations. ([8fceb01](https://bitbucket.org/apisbbva/global-apis-paymentmethods-payments-v0.git/commits/8fceb01))
* **payments:** First commit ([072ffb1](https://bitbucket.org/apisbbva/global-apis-paymentmethods-payments-v0.git/commits/072ffb1))
* **payments:** Updated additionalPaymentField resource ([58238b9](https://bitbucket.org/apisbbva/global-apis-paymentmethods-payments-v0.git/commits/58238b9))
* **payments:** Updated api.raml with new operations and resources. ([067ca98](https://bitbucket.org/apisbbva/global-apis-paymentmethods-payments-v0.git/commits/067ca98))
* **payments:** Updated example files for service resource. ([c0f2855](https://bitbucket.org/apisbbva/global-apis-paymentmethods-payments-v0.git/commits/c0f2855))
* **payments:** Updated types resources to meet GloMo needs. ([27b51ee](https://bitbucket.org/apisbbva/global-apis-paymentmethods-payments-v0.git/commits/27b51ee))
* **service:** Added helpText attribute. ([a1bfad0](https://bitbucket.org/apisbbva/global-apis-paymentmethods-payments-v0.git/commits/a1bfad0))
* **servicePayment:** Added trackingReference field. It help to locate payments on provider´s payments ([a86470c](https://bitbucket.org/apisbbva/global-apis-paymentmethods-payments-v0.git/commits/a86470c))
* **servicePayments:** Added filters on servicePayments GET method. Filters for creationDate and servic ([8dc1374](https://bitbucket.org/apisbbva/global-apis-paymentmethods-payments-v0.git/commits/8dc1374))
* **servicePayments:** Added functionality for billAmount field. ([64ba535](https://bitbucket.org/apisbbva/global-apis-paymentmethods-payments-v0.git/commits/64ba535))
* **services:** Added service-types catalog. Added hypermedia. ([24c9e0a](https://bitbucket.org/apisbbva/global-apis-paymentmethods-payments-v0.git/commits/24c9e0a))
* **services:** Latest changes ([7bee9ab](https://bitbucket.org/apisbbva/global-apis-paymentmethods-payments-v0.git/commits/7bee9ab))
* **services:** QA changes after review. ([ae936a2](https://bitbucket.org/apisbbva/global-apis-paymentmethods-payments-v0.git/commits/ae936a2))
