## The new version of API customers 0.2.0 is available.

Adds a few services to creates, searches, gets and updates customers info. 

See below for bugs solved, user histories and version details.

### Story

* [[GABI1-492](https://globaldevtools.bbva.com/jira/browse/GABI1-492)] - Consultar información básica del cliente utilizando el Documento de Identidad
* [[GABI1-497](https://globaldevtools.bbva.com/jira/browse/GABI1-497)] - Listar Indicadores de Cliente
* [[GABI1-501](https://globaldevtools.bbva.com/jira/browse/GABI1-501)] - Dar de Alta a un nuevo Cliente.
* [[GABI1-502](https://globaldevtools.bbva.com/jira/browse/GABI1-502)] - Modificar la información básica e indicadores de un usuario.
* [[GABI1-567](https://globaldevtools.bbva.com/jira/browse/GABI1-567)] - Consultar datos económicos del cliente
* [[GABI1-568](https://globaldevtools.bbva.com/jira/browse/GABI1-568)] - Consultar contactos del cliente
* [[GABI1-569](https://globaldevtools.bbva.com/jira/browse/GABI1-569)] - Consultar direcciones del cliente
* [[GABI1-587](https://globaldevtools.bbva.com/jira/browse/GABI1-587)] - Mostrar clasificación ( segmentos cliente)
* [[GABI1-665](https://globaldevtools.bbva.com/jira/browse/GABI1-665)] - Obtener los términos y condiciones del cliente
* [[GABI1-666](https://globaldevtools.bbva.com/jira/browse/GABI1-666)] - Ingresar la aprobación de los términos y condiciones por parte del cliente.
## CHANGELOG

### Bug Fixes

* **address:** add method GET PATCH. GABI1-569. ([2d1fb65](https://bitbucket.org/apisbbva/global-apis-people-customers-v0.git/commits/2d1fb65))
* **clasification:** add method GET. GABI1-587. ([aaca15a](https://bitbucket.org/apisbbva/global-apis-people-customers-v0.git/commits/aaca15a))
* **clasification:** add method GET. GABI1-587. ([072afc6](https://bitbucket.org/apisbbva/global-apis-people-customers-v0.git/commits/072afc6))
* **contactDetails:** add method GET PATCH. GABI1-568. ([49070c8](https://bitbucket.org/apisbbva/global-apis-people-customers-v0.git/commits/49070c8))
* **customer:** add method POST GABI1-501. ([ee86849](https://bitbucket.org/apisbbva/global-apis-people-customers-v0.git/commits/ee86849))
* **customer:** add method POST GABI1-501. ([ec1005e](https://bitbucket.org/apisbbva/global-apis-people-customers-v0.git/commits/ec1005e))
* **customer:** add method POST GABI1-501. ([94934a6](https://bitbucket.org/apisbbva/global-apis-people-customers-v0.git/commits/94934a6))
* **customer:** add method POST GABI1-501. ([e161d05](https://bitbucket.org/apisbbva/global-apis-people-customers-v0.git/commits/e161d05))
* **customer:** add method POST GABI1-501. ([0d873dc](https://bitbucket.org/apisbbva/global-apis-people-customers-v0.git/commits/0d873dc))
* **customer:** remove field hasPassedAway customer.raml, customers.raml GABI1-492 ([8f1f137](https://bitbucket.org/apisbbva/global-apis-people-customers-v0.git/commits/8f1f137))
* **customer:** update api.raml, get_200.json, country.raml, customer.raml, documentType.raml, i ([ef064f5](https://bitbucket.org/apisbbva/global-apis-people-customers-v0.git/commits/ef064f5))
* **customer:** update method GET GABI1-492 ([6a166e1](https://bitbucket.org/apisbbva/global-apis-people-customers-v0.git/commits/6a166e1))
* **customer:** update method GET. GABI1-492. ([5c0332e](https://bitbucket.org/apisbbva/global-apis-people-customers-v0.git/commits/5c0332e))
* **economicData:** add method GET. GABI1-567. ([93e16ca](https://bitbucket.org/apisbbva/global-apis-people-customers-v0.git/commits/93e16ca))
* **indicator:** add method GET, POST. GABI1-497 ([f6e3ff2](https://bitbucket.org/apisbbva/global-apis-people-customers-v0.git/commits/f6e3ff2))
* **indicator:** add method GET, POST. GABI1-497 ([fa8e6a0](https://bitbucket.org/apisbbva/global-apis-people-customers-v0.git/commits/fa8e6a0))
* **indicator:** add method GET, POST. GABI1-497 ([d73e499](https://bitbucket.org/apisbbva/global-apis-people-customers-v0.git/commits/d73e499))
* **merge:** merge ([e595eca](https://bitbucket.org/apisbbva/global-apis-people-customers-v0.git/commits/e595eca))
* **merge:** merge ([b93256a](https://bitbucket.org/apisbbva/global-apis-people-customers-v0.git/commits/b93256a))
* **merge:** merge ([ac4c23f](https://bitbucket.org/apisbbva/global-apis-people-customers-v0.git/commits/ac4c23f))
* **merge:** merge ([fc64f2a](https://bitbucket.org/apisbbva/global-apis-people-customers-v0.git/commits/fc64f2a))
* **merge:** merge ([08d9634](https://bitbucket.org/apisbbva/global-apis-people-customers-v0.git/commits/08d9634))
* **merge:** merge ([c730004](https://bitbucket.org/apisbbva/global-apis-people-customers-v0.git/commits/c730004))
* **merge:** merge ([a5ade67](https://bitbucket.org/apisbbva/global-apis-people-customers-v0.git/commits/a5ade67))
* **merge:** merge ([95df6d3](https://bitbucket.org/apisbbva/global-apis-people-customers-v0.git/commits/95df6d3))
* **merge:** merge ([6599a3d](https://bitbucket.org/apisbbva/global-apis-people-customers-v0.git/commits/6599a3d))
* **post:** delete bindingdefinition for post ([888ca38](https://bitbucket.org/apisbbva/global-apis-people-customers-v0.git/commits/888ca38))
* **rename:** renamed geographicGroupsType type by this geographicGroupType in json examples ([8efbb84](https://bitbucket.org/apisbbva/global-apis-people-customers-v0.git/commits/8efbb84))
* **rename:** renamed this type by this bgeographicGroupsType in json examples ([c433010](https://bitbucket.org/apisbbva/global-apis-people-customers-v0.git/commits/c433010))
* **rename:** renamed this type by this bgeographicGroupsType in json examples ([94dbd77](https://bitbucket.org/apisbbva/global-apis-people-customers-v0.git/commits/94dbd77))
* **review:** add expands ([457367f](https://bitbucket.org/apisbbva/global-apis-people-customers-v0.git/commits/457367f))
* **review:** minor changes descriptions ([ca7585a](https://bitbucket.org/apisbbva/global-apis-people-customers-v0.git/commits/ca7585a))
* **revision:** revision pull request GABI1-492 ([620c424](https://bitbucket.org/apisbbva/global-apis-people-customers-v0.git/commits/620c424))
* **revision:** revision pull request GABI1-492 ([a4cef9a](https://bitbucket.org/apisbbva/global-apis-people-customers-v0.git/commits/a4cef9a))
* **revision:** revision pull request GABI1-569 ([c777f9b](https://bitbucket.org/apisbbva/global-apis-people-customers-v0.git/commits/c777f9b))
* **sortable:** change name ([fbf68d2](https://bitbucket.org/apisbbva/global-apis-people-customers-v0.git/commits/fbf68d2))
* **term:** add method GET PATCH. GABI1-666. ([29b0ca0](https://bitbucket.org/apisbbva/global-apis-people-customers-v0.git/commits/29b0ca0))
* **term:** add method GET, POST. GABI1-666. ([97c6af8](https://bitbucket.org/apisbbva/global-apis-people-customers-v0.git/commits/97c6af8))

### Features

* **address:** update method GET, PATCH. GABI1-569 ([d9cbf08](https://bitbucket.org/apisbbva/global-apis-people-customers-v0.git/commits/d9cbf08))
* **contactDetails:** update method GET PATCH GABI1-568 ([efa9bcb](https://bitbucket.org/apisbbva/global-apis-people-customers-v0.git/commits/efa9bcb))
* **economicData:** update method GET. GABI1-567 ([ca4e4fe](https://bitbucket.org/apisbbva/global-apis-people-customers-v0.git/commits/ca4e4fe))
* **trait:** add field ([2351a3b](https://bitbucket.org/apisbbva/global-apis-people-customers-v0.git/commits/2351a3b))
