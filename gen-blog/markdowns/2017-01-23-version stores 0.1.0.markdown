## The new API Stores 0.1.0 is available.

This API includes all the available services to manage information about stores. A store is a business company based on the buy, sale or exchange of goods and services.

### New methods
* GET /stores
* GET /stores/{store-id}
* GET /operations
* GET /operations/{operation-id}
* GET /balances
<br>

See below for bugs solved, user histories and version details.

### Feature

* [[GABI1-636](https://globaldevtools.bbva.com/jira/browse/GABI1-636)] - Funcionalidades sobre tiendas

## CHANGELOG

### Bug Fixes

* **aggregations:** Updated examples ([57e7723](https://bitbucket.org/apisbbva/global-apis-enterprises-stores-v0.git/commits/57e7723))
* **all:** Fixed some errors found on Pair revision - GABI1-30 Jira US ([3d80bee](https://bitbucket.org/apisbbva/global-apis-enterprises-stores-v0.git/commits/3d80bee))
* **api:** Commos refactor ([7f40a70](https://bitbucket.org/apisbbva/global-apis-enterprises-stores-v0.git/commits/7f40a70))
* **balance:** Changed 'sales' node type to 'balanceOperation.balanceOperation[]' ([ce8f79e](https://bitbucket.org/apisbbva/global-apis-enterprises-stores-v0.git/commits/ce8f79e))
* **balance:** Changed attributes and examples ([6c18ae0](https://bitbucket.org/apisbbva/global-apis-enterprises-stores-v0.git/commits/6c18ae0))
* **balances:** added more examples ([d576962](https://bitbucket.org/apisbbva/global-apis-enterprises-stores-v0.git/commits/d576962))
* **balances:** added more examples ([cd126b2](https://bitbucket.org/apisbbva/global-apis-enterprises-stores-v0.git/commits/cd126b2))
* **balances:** added more examples ([b668f39](https://bitbucket.org/apisbbva/global-apis-enterprises-stores-v0.git/commits/b668f39))
* **balances:** changed property 'date' for 'fromDate' and 'toDate' ([20f8f9b](https://bitbucket.org/apisbbva/global-apis-enterprises-stores-v0.git/commits/20f8f9b))
* **balances:** Updated 'balances' resource ([f1cf1a3](https://bitbucket.org/apisbbva/global-apis-enterprises-stores-v0.git/commits/f1cf1a3))
* **balances:** Updated 'balances' resource ([7c6dd32](https://bitbucket.org/apisbbva/global-apis-enterprises-stores-v0.git/commits/7c6dd32))
* **balances:** Updated 'balances' resource ([6e6ce3a](https://bitbucket.org/apisbbva/global-apis-enterprises-stores-v0.git/commits/6e6ce3a))
* **balances:** updated examples ([4c564dd](https://bitbucket.org/apisbbva/global-apis-enterprises-stores-v0.git/commits/4c564dd))
* **balances:** updated examples ([fdc32ea](https://bitbucket.org/apisbbva/global-apis-enterprises-stores-v0.git/commits/fdc32ea))
* **cardPayment:** Set required attributes to false ([99dca6a](https://bitbucket.org/apisbbva/global-apis-enterprises-stores-v0.git/commits/99dca6a))
* **commerces:** Cambios en los dataTypes, headers e incluir spin-case en uriParameters ([de3c538](https://bitbucket.org/apisbbva/global-apis-enterprises-stores-v0.git/commits/de3c538))
* **commerces:** Change attribute "image" to "images" ([3f1ac57](https://bitbucket.org/apisbbva/global-apis-enterprises-stores-v0.git/commits/3f1ac57))
* **commerces:** Change attribute image to type array ([c04a1f6](https://bitbucket.org/apisbbva/global-apis-enterprises-stores-v0.git/commits/c04a1f6))
* **commerces:** Changes due to HQ review. ([e343903](https://bitbucket.org/apisbbva/global-apis-enterprises-stores-v0.git/commits/e343903))
* **commerces:** extracted types to commons ([fedd7c7](https://bitbucket.org/apisbbva/global-apis-enterprises-stores-v0.git/commits/fedd7c7))
* **commerces:** Refactorización de la versión 0.8 a 1.0 ([a4aad6e](https://bitbucket.org/apisbbva/global-apis-enterprises-stores-v0.git/commits/a4aad6e))
* **commerces:** the output attributes are as OPTIONAL ([6d19da2](https://bitbucket.org/apisbbva/global-apis-enterprises-stores-v0.git/commits/6d19da2))
* **commerces-v1:** Added the new structure of folders to the errors. GABI1-119 ([7e581ee](https://bitbucket.org/apisbbva/global-apis-enterprises-stores-v0.git/commits/7e581ee))
* **enum:** rename geographic groups types ([4fae54b](https://bitbucket.org/apisbbva/global-apis-enterprises-stores-v0.git/commits/4fae54b))
* **examples:** Updated dates to timestamp ([1bbf23a](https://bitbucket.org/apisbbva/global-apis-enterprises-stores-v0.git/commits/1bbf23a))
* **examples:** Updated dates to timestamp ([94bf244](https://bitbucket.org/apisbbva/global-apis-enterprises-stores-v0.git/commits/94bf244))
* **examples:** Updated dates to timestamp ([7a7c42e](https://bitbucket.org/apisbbva/global-apis-enterprises-stores-v0.git/commits/7a7c42e))
* **examples:** Updated examples values ([62a717d](https://bitbucket.org/apisbbva/global-apis-enterprises-stores-v0.git/commits/62a717d))
* **feat:** Refactored with Global API Catalog instructions ([1681bf5](https://bitbucket.org/apisbbva/global-apis-enterprises-stores-v0.git/commits/1681bf5))
* **feat:** Refactored with Global API Catalog instructions ([7c1f7f8](https://bitbucket.org/apisbbva/global-apis-enterprises-stores-v0.git/commits/7c1f7f8))
* **feat:** Refactored with Global API Catalog instructions ([742d718](https://bitbucket.org/apisbbva/global-apis-enterprises-stores-v0.git/commits/742d718))
* **pointOfSaleGroup:** removed references to pointOfSaleGroup ([3137d57](https://bitbucket.org/apisbbva/global-apis-enterprises-stores-v0.git/commits/3137d57))
* **raml:** Delete the resourceTypes "read-collection and read-collection-item" ([63fd3ad](https://bitbucket.org/apisbbva/global-apis-enterprises-stores-v0.git/commits/63fd3ad))
* **raml:** review ([5838fa4](https://bitbucket.org/apisbbva/global-apis-enterprises-stores-v0.git/commits/5838fa4))
* **raml:** TPV by POS ([3b9d64a](https://bitbucket.org/apisbbva/global-apis-enterprises-stores-v0.git/commits/3b9d64a))
* **rename:** rename endpoints ([df42d05](https://bitbucket.org/apisbbva/global-apis-enterprises-stores-v0.git/commits/df42d05))
* **rename:** rename queryParam expand ([737bf72](https://bitbucket.org/apisbbva/global-apis-enterprises-stores-v0.git/commits/737bf72))
* **rename:** views ([dfbc454](https://bitbucket.org/apisbbva/global-apis-enterprises-stores-v0.git/commits/dfbc454))
* **rename:** views ([3c9af6d](https://bitbucket.org/apisbbva/global-apis-enterprises-stores-v0.git/commits/3c9af6d))
* Adapted RAML for doc parser ([6a79a96](https://bitbucket.org/apisbbva/global-apis-enterprises-stores-v0.git/commits/6a79a96))
* Adapted RAML for doc parser ([76db8ed](https://bitbucket.org/apisbbva/global-apis-enterprises-stores-v0.git/commits/76db8ed))
* added originAmount to operation & operations ([0744659](https://bitbucket.org/apisbbva/global-apis-enterprises-stores-v0.git/commits/0744659))
* channel -> paymentChannel ([13af19f](https://bitbucket.org/apisbbva/global-apis-enterprises-stores-v0.git/commits/13af19f))
* enum balances, /stores/{store-id}, rutas ([dcbdee1](https://bitbucket.org/apisbbva/global-apis-enterprises-stores-v0.git/commits/dcbdee1))
* location ([4ba54ef](https://bitbucket.org/apisbbva/global-apis-enterprises-stores-v0.git/commits/4ba54ef))
* **sortable:** change name ([3e13019](https://bitbucket.org/apisbbva/global-apis-enterprises-stores-v0.git/commits/3e13019))
* **stores:** Updated examples ([66374ca](https://bitbucket.org/apisbbva/global-apis-enterprises-stores-v0.git/commits/66374ca))
* operationId -> id ([ace6e4f](https://bitbucket.org/apisbbva/global-apis-enterprises-stores-v0.git/commits/ace6e4f))
* postedTimestamp -> postedDate ([658028c](https://bitbucket.org/apisbbva/global-apis-enterprises-stores-v0.git/commits/658028c))
* **verision:** change version ([7b4ac1f](https://bitbucket.org/apisbbva/global-apis-enterprises-stores-v0.git/commits/7b4ac1f))
* **version:** delete changelog ([14c1bf6](https://bitbucket.org/apisbbva/global-apis-enterprises-stores-v0.git/commits/14c1bf6))

### Features

* **balances:** added balances endpoint ([a41114e](https://bitbucket.org/apisbbva/global-apis-enterprises-stores-v0.git/commits/a41114e))
* **branches:** Added branches information. Added web attribute to commerce structure. ([4854225](https://bitbucket.org/apisbbva/global-apis-enterprises-stores-v0.git/commits/4854225))
* **commerce:** First readme ([bb13899](https://bitbucket.org/apisbbva/global-apis-enterprises-stores-v0.git/commits/bb13899))
* **commerces-v0:** RC2 RAML revision ([f8b6499](https://bitbucket.org/apisbbva/global-apis-enterprises-stores-v0.git/commits/f8b6499))
* **operations:** added operations and /operations/{id} ([b924bbe](https://bitbucket.org/apisbbva/global-apis-enterprises-stores-v0.git/commits/b924bbe))
* **refactor:** types ([194773b](https://bitbucket.org/apisbbva/global-apis-enterprises-stores-v0.git/commits/194773b))
* **refactor:** types ([18238fc](https://bitbucket.org/apisbbva/global-apis-enterprises-stores-v0.git/commits/18238fc))
* **repositories:** renames repositories ([10571b5](https://bitbucket.org/apisbbva/global-apis-enterprises-stores-v0.git/commits/10571b5))
* **stores:** Added stores resources and modified operation/pointOfSale/paymentMethod types ([c582e5b](https://bitbucket.org/apisbbva/global-apis-enterprises-stores-v0.git/commits/c582e5b))
* **view:** Dynamic routing ([94c001b](https://bitbucket.org/apisbbva/global-apis-enterprises-stores-v0.git/commits/94c001b))
* added new attributes ([88f977c](https://bitbucket.org/apisbbva/global-apis-enterprises-stores-v0.git/commits/88f977c))
* added new attributes ([176f93a](https://bitbucket.org/apisbbva/global-apis-enterprises-stores-v0.git/commits/176f93a))
