## The new API Stores 0.1.0 is available.

This API includes any service related to insurance to exchange rate.

### New methods
* GET /exchange-rate-vouchers
<br>

See below for bugs solved, user histories and version details.

### Feature

* [[GABI1-1312](https://globaldevtools.bbva.com/jira/browse/GABI1-1312)] - Boletas de seguros de cambio de divisa

## CHANGELOG

### Bug Fixes

* agregados cambios del pull request 5 ([a8cda78](https://bitbucket.org/apisbbva/global-apis-products-exchangeratevouchers-v0.git/commits/a8cda78))
* agregados required a objetos raiz y modificado ticket -> voucher ([13f2870](https://bitbucket.org/apisbbva/global-apis-products-exchangeratevouchers-v0.git/commits/13f2870))
* arreglo tipo id, Required correcciones, Añadido createDate para alinear con ASO ([651ce9d](https://bitbucket.org/apisbbva/global-apis-products-exchangeratevouchers-v0.git/commits/651ce9d))
* correcciones de Jorge y eliminada paginación ([f2c69ff](https://bitbucket.org/apisbbva/global-apis-products-exchangeratevouchers-v0.git/commits/f2c69ff))
* correcciones de nombres y url ([9e9bd8f](https://bitbucket.org/apisbbva/global-apis-products-exchangeratevouchers-v0.git/commits/9e9bd8f))
* correcciones indicadas ([fc0edbb](https://bitbucket.org/apisbbva/global-apis-products-exchangeratevouchers-v0.git/commits/fc0edbb))
* **exchangeRateVouchers:** changed example url to meet standard. ([3acdffc](https://bitbucket.org/apisbbva/global-apis-products-exchangeratevouchers-v0.git/commits/3acdffc))
* **exchangeRateVouchers:** fixed title and base url. ([7c3f57d](https://bitbucket.org/apisbbva/global-apis-products-exchangeratevouchers-v0.git/commits/7c3f57d))
* **raml:** review ([b7c06df](https://bitbucket.org/apisbbva/global-apis-products-exchangeratevouchers-v0.git/commits/b7c06df))
* eliminados verbos de description sobrantes ([8705e6c](https://bitbucket.org/apisbbva/global-apis-products-exchangeratevouchers-v0.git/commits/8705e6c))
* pullrequest comments ([ac84780](https://bitbucket.org/apisbbva/global-apis-products-exchangeratevouchers-v0.git/commits/ac84780))

### Features

* **exchangeratevouchers:** add ENUM ([cea0dd6](https://bitbucket.org/apisbbva/global-apis-products-exchangeratevouchers-v0.git/commits/cea0dd6))
* **exchangeratevouchers:** correction object type commit ([3cfa79b](https://bitbucket.org/apisbbva/global-apis-products-exchangeratevouchers-v0.git/commits/3cfa79b))
* **exchangeratevouchers:** correction type commit ([3019a29](https://bitbucket.org/apisbbva/global-apis-products-exchangeratevouchers-v0.git/commits/3019a29))
* **exchangeratevouchers:** correction type commit ([ce1d481](https://bitbucket.org/apisbbva/global-apis-products-exchangeratevouchers-v0.git/commits/ce1d481))
* **exchangeratevouchers:** initial commit ([b5e83ec](https://bitbucket.org/apisbbva/global-apis-products-exchangeratevouchers-v0.git/commits/b5e83ec))
* **exchangeratevouchers:** initial commit ([6cfe877](https://bitbucket.org/apisbbva/global-apis-products-exchangeratevouchers-v0.git/commits/6cfe877))
* **exchangeRateVouchers:** Criterio unificado tipos de atributos ([11c7e46](https://bitbucket.org/apisbbva/global-apis-products-exchangeratevouchers-v0.git/commits/11c7e46))
* Correciones de clases y llamada ([2aaf6e4](https://bitbucket.org/apisbbva/global-apis-products-exchangeratevouchers-v0.git/commits/2aaf6e4))
