## The new version of API contact-book 0.1.0 is available.

### New methods added
* GET /contacts
* POST /contacts
* GET /contacts/{contact-id}
* POST /contacts/{contact-id}
* PATCH /contacts/{contact-id}
* DELETE /contacts/{contact-id}
* GET /contacts/{contact-id}/contact-information
* POST /contacts/{contact-id}/contact-information
* GET /contacts/{contact-id}/contact-information/{contact-information-id}
* POST /contacts/{contact-id}/contact-information/{contact-information-id}
* PATCH /contacts/{contact-id}/contact-information/{contact-information-id}
* DELETE /contacts/{contact-id}/contact-information/{contact-information-id}
* GET /contacts/{contact-id}/products
* POST /contacts/{contact-id}/products
* GET /contacts/{contact-id}/products/{product-id}
* POST /contacts/{contact-id}/products/{product-id}
* PATCH /contacts/{contact-id}/products/{product-id}
* DELETE /contacts/{contact-id}/products/{product-id}
<br>

See below for bugs solved, user histories and version details.

### Feature

*   [[GABI1-1071](https://globaldevtools.bbva.com/jira/browse/GABI1-1070)] - Operaciones con las cuentas de destino para transferencias

## CHANGELOG

### Bug Fixes

* **api:** add view file ([5395b82](https://bitbucket.org/apisbbva/global-apis-people-contactsbook-v0.git/commits/5395b82))
* **atributes:** add surname and birth date ([1340475](https://bitbucket.org/apisbbva/global-apis-people-contactsbook-v0.git/commits/1340475))
* **contact:** person and contact information ([25f4d7e](https://bitbucket.org/apisbbva/global-apis-people-contactsbook-v0.git/commits/25f4d7e))
* **html:** add view ([82c5c15](https://bitbucket.org/apisbbva/global-apis-people-contactsbook-v0.git/commits/82c5c15))
* **person:** person in contact ([73e767e](https://bitbucket.org/apisbbva/global-apis-people-contactsbook-v0.git/commits/73e767e))
* **person:** person in contact ([1ddfbe1](https://bitbucket.org/apisbbva/global-apis-people-contactsbook-v0.git/commits/1ddfbe1))
* **person:** person in contact ([d5f9082](https://bitbucket.org/apisbbva/global-apis-people-contactsbook-v0.git/commits/d5f9082))
* **revision:** ajust types ([c1d052d](https://bitbucket.org/apisbbva/global-apis-people-contactsbook-v0.git/commits/c1d052d))
