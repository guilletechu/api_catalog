#!/usr/bin/env node
'use strict';

var meow = require('meow'),
    commiter = require('../bump-version-commit/app.js'),
    pusher = require('../bump-version-push/app.js'),
    log = require('../logger/logger.js'),
    path = require('path');

var cli = meow({
    help: [
        'Usage',
        '  node cli.js -r <path-to-raml-repository> -v <version>',
        '',
        'Example',
        '  node cli.js -r ../../cards-v1/ -v 0.1.0',
        '',
    ]
});

var repositoryPath = cli.flags.r,
    version = cli.flags.v;

if (!repositoryPath) {
    log.error('Path is not specified');
    cli.showHelp();
    process.exit(1);
}
if (!version) {
    log.error('Version is not specified');
    cli.showHelp();
    process.exit(1);
}
var versionRegexOutput = new RegExp(/^(\d+)\.(\d+)\.(\d+)$/).exec(version);
if (versionRegexOutput === null) {
  log.error('Invalid version format.');
  process.exit(1);
}
var config = {path: path.resolve(repositoryPath), version: version};
commiter(config)
  .then(pusher)
  .catch(function(error){
    log.error('There was an error in bump proccess.');
    if(error) log.error(error);
  });
