#!/usr/bin/env node
var https = require('https'),
    Q = require('q'),
    slack = require('../gen-slack-notification/app.js'),
    log = require('../logger/logger.js'),
    fs = require('fs'),
    slackConfig = JSON.parse(fs.readFileSync(__dirname + '/default-slack-config.json', 'utf8'));
    exec = require('child_process').exec;

function pusher(config){
  var repositoryName = config.path.split('\\').pop();
  function resolvePromiseFunction(promise) {
      return function (error, stdout, stderr) {
          if (error) log.error(error);
          if (stderr) log.warning(stderr);
          if (stdout) log.info(stdout);
          if (error === null) return promise.resolve();
          else promise.reject();
      };
  }
  function deferExec(execCommand) {
      var deferred = Q.defer();
      log.info('Executing: ' + execCommand);
      exec(execCommand, {cwd: config.path}, resolvePromiseFunction(deferred));
      return deferred.promise;
  }
  function pushMaster(){
    return deferExec('git push origin master --follow-tags');
  }
  function pushDevelop(){
    return deferExec('git push origin develop');
  }
  function notifySlack(){
    log.info('Notify slack...');
    var deferred = Q.defer();

    function notificateSlack(channel){
      var text = 'New version (' + config.version + ') for repository (<https://bitbucket.org/PaaSHolding/'+repositoryName+'|'+repositoryName+'>) was created';
      var deferred = Q.defer();
      slack.notify(slackConfig.token,channel,text,slackConfig.user,slackConfig.emoji)
      .then(function(result){
        deferred.resolve(result);
      })
      .catch(function(err){
        deferred.reject(err);
      });
      return deferred.promise;
    }
    function obtainChannel(repositoryName){
      var deferred = Q.defer();
      var channelName = 'api-' + repositoryName.split('-')[0];
      slack.obtainChannel(slackConfig.token, channelName)
      .then(function(channel){
        deferred.resolve(channel);
      })
      .catch(function(err){
        deferred.reject(err);
      });
      return deferred.promise;
    }

    obtainChannel(repositoryName)
      .then(notificateSlack)
      .then(function(response){
        log.info('Message sent: ', response.message.text);
      }).catch(function(err){
        log.error(err);
      });
    return deferred.promise;
  }
  function deferConfig(){
    var deferred = Q.defer();
    deferred.resolve(config);
    return deferred.promise;
  }
  return pushMaster()
    .then(pushDevelop)
    .then(notifySlack)
    .then(deferConfig)
    .catch(function(err){
      log.error("There was an error pushing the repo");
      if(err) log.error(err);
    });
}

module.exports = pusher;
