#!/usr/bin/env node
'use strict';

var app = require('./app.js'),
    meow = require('meow'),
    log = require('../logger/logger.js'),
    path = require('path');

    var cli = meow({
        help: [
            'Usage',
            '  node cli.js -r <path-to-raml-repository>',
            '',
            'Example',
            '  node cli.js -r ../../cards-v1/',
            '',
        ]
    });

    var repositoryPath = cli.flags.r;

    if (!repositoryPath) {
        log.error('Path is not specified');
        cli.showHelp();
        process.exit(1);
    }

    app(path.resolve(repositoryPath));
