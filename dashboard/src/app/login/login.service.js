(function () {
    'use strict';

    /** @ngInject */
    function LoginService($http, $q) {

        function getStructure(user, pass) {
            var deferred = $q.defer();
            $http({
                method: "GET",
                url: 'https://bitbucket.org/apisbbva/paas-toolbox/raw/master/gen-site/src/structure.json',
                headers: {
                    'Authorization': 'Basic ' + btoa(user + ':' + pass)
                }
            }).then(function (data) {
                deferred.resolve(data.data)
            });
            return deferred.promise;
        }

        return {
            getStructure: getStructure
        }
    }

    angular
        .module('dashboard')
        .factory('LoginService', LoginService);
})();