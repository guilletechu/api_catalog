(function () {
    'use strict';

    /** @ngInject */
    function VersionedService($http, $q) {

        function getStructure(apiName) {
            return localStorage.getItem('catalog');
        }

        function getInfoRepoVersion(repoName) {
            var deferred = $q.defer();
            $http({
                method: "GET",
                url: 'https://bitbucket.org/apisbbva/' + repoName + '/raw/master/versions.json',
                headers: {
                    'Authorization': 'Basic ' + localStorage.getItem('pth')
                }
            }).then(function (data) {
                deferred.resolve(data.data)
            });
            return deferred.promise;
        }

        return {
            getStructure: getStructure,
            getInfoRepoVersion: getInfoRepoVersion
        }
    }

    angular
        .module('dashboard')
        .factory('VersionedService', VersionedService);
})();