(function() {
    'use strict';

    /** @ngInject */
    function VersionedController(VersionedService) {
        var vm = this,
            catalog = JSON.parse(VersionedService.getStructure());
        
        vm.repositories = [];
        
        angular.forEach(catalog, function(group) {
            angular.forEach(group.sections, function(section) {
                angular.forEach(section.domains, function(domain) {
                    vm.repositories.push(domain['repository-url'].replace('https://bitbucket.org/apisbbva/', '').toUpperCase())
                });
            });
        });

        vm.getInfoRepoVersion = function () {
            VersionedService.getInfoRepoVersion(vm.repoName.toLowerCase())
                .then(function(data) {
                    vm.infoVersions = data;
                })
        }
    }

    angular
        .module('dashboard')
        .controller('VersionedController', VersionedController);
})();
