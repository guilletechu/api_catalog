/* global ipcRenderer */
(function () {
    'use strict';

    /** @ngInject */
    function IndexService() {

        function launcher(elementId, windowEvent, electronEvent) {
            var element = angular.element(document.querySelector(elementId));

            element.on(windowEvent, function() {
                ipcRenderer.send(electronEvent);
            });
        }

        return {
            launcher: launcher
        }
    }

    angular
        .module('dashboard')
        .factory('IndexService', IndexService);
})();