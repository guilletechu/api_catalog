(function() {
    'use strict';

    /** @ngInject */
    function DevelopController(IndexService) {
        IndexService.launcher('#launch', 'click', 'launchSite');
    }
    angular
        .module('dashboard')
        .controller('DevelopController', DevelopController);
})();
