(function() {
    'use strict';

    const electron = require('electron'),
          path = require('path'),
          launcher = require(path.resolve(__dirname, 'modules/launcher.js')),
          app = electron.app,
          BrowserWindow = electron.BrowserWindow;
    let mainWindow;

    function createWindow() {
        mainWindow = new BrowserWindow({
            fullscreen: true
        });
        mainWindow.loadURL('file://' + __dirname + '/dist/index.html');

        // Open the DevTools.
        mainWindow.webContents.openDevTools()


        mainWindow.on('closed', () => {
            mainWindow = null
        });
    }
    app.on('ready', createWindow);

    app.on('window-all-closed', () => {
        if (process.platform !== 'darwin') {
            app.quit();
        }
    });
    app.on('activate', () => {
        if (mainWindow === null) {
            createWindow();
        }
    });

    launcher.launcherSpawn('launchSite', 'gulp', ['serve', '--gulpfile'], './../gen-site/gulpfile.js');
})();
