'use strict';

const electron = require('electron'),
      ipcMain = require('electron').ipcMain,
      spawn = require('child_process').spawn;

function launcherSpawn(event, command, args, script) {
    ipcMain.on(event, function() {
        let spawnArgs = [].concat(args || [], script)
        spawn(command, spawnArgs, {
            cwd: global
        });
    });
}

function launcherExec(event, command, args, script) {
    // ipcMain.on('launchSite', function() {
    //     spawn('gulp', ['serve', '--gulpfile', './../gen-site/gulpfile.js'], {
    //         cwd: global
    //     });
    // });
}

module.exports = {
    launcherSpawn: launcherSpawn,
    launcherExec: launcherExec
};